<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List"%>
<%@page import="model.Flight"%>
<%@page import="services.LoginService"%>
<%@page import="model.User" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin</title>
</head>

<body>
<%
String username = null;
Cookie[] cookies = request.getCookies();
if(cookies !=null){
for(Cookie cookie : cookies){
	if(cookie.getName().equals("username")) username = cookie.getValue();
}
}
//if(username == null) response.sendRedirect("login");
LoginService login = new LoginService();
List<User> users = login.getAll();

for(User user : users){
	if(user.getUsername().equals(username) && user.getType().equals("user")){
		response.sendRedirect("login");
	}
}

List<Flight> flights = (List<Flight>) session.getAttribute("flights");
%>
	<form method=POST>
		<h1>Admin home</h1>
		<table>
			<tr>
				<td><label for="number">Number</label> <input
					type="text" name="number" value=""></td>


				<td><label for="depCity">Departure city</label> <input type="text"
					name="depCity" value=""></td>


				<td><label for="depTime">Departure time</label> <input type="datetime-local" name="depTime"
					value=""></td>

				<td><label for="arrCity">Arrival city</label> <input
					type="text" name="arrCity" value=""></td>

				<td><label for="arrTime">Arrival Time</label> <input type="datetime-local"
					name="arrTime" value=""></td>
			
				<td><label for="pType">Plane type</label> <input type="text"
					name="pType" value=""></td>
			</tr>
		</table>
		
		
		<table>
		
			<tr>
				<td><label for="searchBy">Search by:</label> <select name="searchBy">
						<option value="Number">Number</option>
						<option value="Departure city">Departure city</option>
						<option value="Departure time">Departure time</option>
						<option value="Arrival city">Arrival city</option>
						<option value="Arrival time">Arrival time</option>
						<option value="Plane type">Plane type</option>
				</select></td>
				<td><input type="submit" value="Search" name="search"></td>
				<td><input type="submit" value="Create" name="create"></td>
				
			</tr>
			
		</table>
		<table border="1">
			<thead>
				<tr>
					<th>Number</th>
					<th>Departure city</th>
					<th>Departure time</th>
					<th>Arrival city</th>
					<th>Arrival time</th>
					<th>Plane type</th>
					<th>Selection</th>
				</tr>
			</thead>
			<%
                     for (Flight flight : flights) {
                 %>
			<tbody>
					<tr>
						<td><%=flight.getNumber()%></td>
						<td><%=flight.getDepartureStation().getName()%></td>
						<td><%=flight.getDeparture()%></td>
						<td><%=flight.getArrivalStation().getName()%></td>
						<td><%=flight.getArrival()%></td>
						<td><%=flight.getAirplaneType()%></td>
						<td><input type="checkbox" name="<%=flight.getId()%>Checkbox"></td>
					</tr>
				<%}%>
			</tbody>
		</table>

		<table>
			<tr>
				<td><input type="submit" name="delete" value="Delete"></td>
			</tr>
			<tr>
				<td><label for="updateDrop">Update:</label> <select
					name="updateDrop">
						<option value="Number">Number</option>
						<option value="Departure city">Departure city</option>
						<option value="Departure time">Departure time</option>
						<option value="Arrival city">Arrival city</option>
						<option value="Arrival time">Arrival time</option>
						<option value="Plane type">Plane type</option>
				</select></td>

				<td><label for="toUpdate">Desired update:</label> <input
					type="text" name="toUpdate"></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
			
		</table>
	</form>
</body>
</html>