<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="model.Flight"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Flights</title>
</head>
<body>

<%
String username = null;
Cookie[] cookies = request.getCookies();
if(cookies !=null){
for(Cookie cookie : cookies){
	if(cookie.getName().equals("username")) username = cookie.getValue();
}
}
//if(username == null) response.sendRedirect("login");
	List<Flight> flights = (List<Flight>) session.getAttribute("flights");
%>
	<h1>Flights</h1>
	<form  method=POST>
		<table>
			<tr>
			<td>
				<label for="depCity">Departure City</label> <select name="depCity">
				 <% for (Flight flight : flights) { %>
						<option value=<%=flight.getDepartureStation().getName()%>><%=flight.getDepartureStation().getName()%></option>
						<%} %>
				</select>
				</td>

				<td><label for="arrTime">Arrival Time</label> <input name="arrTime" type="datetime-local"S></td>
				
				<td><label for="searchBy">Search by:</label> <select name="searchBy">
						<option value="Departure city">Departure city</option>
						<option value="Arrival time">Arrival time</option>
				</select></td>
				
				<td><input type="submit" value="Search" name="search"></td>
				
			</tr>
		</table>
		</form>
	
		<table border="1">
			<thead>
				<tr>
					<th>Number</th>
					<th>Departure city</th>
					<th>Departure time</th>
					<th>Arrival city</th>
					<th>Arrival time</th>
					<th>Plane type</th>
				</tr>
			</thead>
					<%
                     for (Flight flight : flights) {
                 %>
			<tbody>
					<tr>
						<td><%=flight.getNumber()%></td>
						<td><%=flight.getDepartureStation().getName()%></td>
						<td><%=flight.getDeparture()%></td>
						<td><%=flight.getArrivalStation().getName()%></td>
						<td><%=flight.getArrival()%></td>
						<td><%=flight.getAirplaneType()%></td>
					</tr>
				<%}%>
			</tbody>
		</table>
</body>
</html>