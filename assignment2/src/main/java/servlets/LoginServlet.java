package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import model.City;
import repositories.CityRepository;
import services.LoginService;

@WebServlet(name = "LoginServlet", urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private LoginService loginService;

	public LoginServlet() {
		super();
		this.loginService = new LoginService();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String loginType = loginService.determineLoginType(username, password);
		Cookie loginCookie = new Cookie("user",username);
		loginCookie.setMaxAge(30*60);
		response.addCookie(loginCookie);
		System.out.println(loginType);
		response.sendRedirect(loginType);
		/*if(loginType.equals("admin")) {
			response.sendRedirect("admin");
			return;
		}
		else {
			if(loginType.equals("user")) {
				response.sendRedirect("user");
				//request.getRequestDispatcher("/WEB-INF/jsp/employeePage.jsp").forward(request, response);
				return;
			}
			else {
				response.sendRedirect("login");
				return;
			}
		}*/
	}

}
