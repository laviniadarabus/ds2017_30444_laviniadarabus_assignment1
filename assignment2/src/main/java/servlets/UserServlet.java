package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Flight;
import services.UserService;

@WebServlet(name = "UserServlet", urlPatterns = { "/user" })
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UserService userService;

	public UserServlet() {
		super();
		this.userService = new UserService();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		for(Cookie c : request.getCookies()) {
//			System.out.println(c.getValue());
//		}
		request.getSession().setAttribute("flights", this.userService.getFlights());
		request.getRequestDispatcher("/WEB-INF/jsp/employeePage.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String searchBy = request.getParameter("searchBy");
		String depCityName = request.getParameter("depCity");
		String arrivalTime = request.getParameter("arrTime");
		List<Flight> searchResult = this.userService.performSearch(searchBy, depCityName, arrivalTime);
		request.getSession().setAttribute("flights", searchResult);
		System.out.println(searchBy);
		System.out.println(depCityName);
		System.out.println(arrivalTime);
		response.sendRedirect("user");
	}

}
