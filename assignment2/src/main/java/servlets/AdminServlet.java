package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Flight;
import model.User;
import services.AdminService;
import services.LoginService;

@WebServlet(name = "AdminServlet", urlPatterns = { "/admin" })
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private AdminService adminService;
	List<Flight> flights;

	public AdminServlet() {
		super();
		this.adminService = new AdminService();
		this.flights = this.adminService.getFlights();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.getSession().setAttribute("flights", flights);
		request.getRequestDispatcher("/WEB-INF/jsp/adminPage.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		flights = this.adminService.getFlights();
		
		String planeNumber = request.getParameter("number");
		String depCityName = request.getParameter("depCity");
		String arrCityName = request.getParameter("arrCity");
		String depTime = request.getParameter("depTime");
		String arrTime = request.getParameter("arrTime");
		String pType = request.getParameter("pType");
		
		String searchBy = request.getParameter("searchBy");
		
		String toUpdate = request.getParameter("toUpdate");
		String updateDrop = request.getParameter("updateDrop");
		
		if(request.getParameter("search") != null) {
			flights = this.adminService.performSearch(planeNumber, depCityName, arrCityName, depTime, arrTime, pType, searchBy);
		}
		
		if(request.getParameter("create") != null) {
			flights = this.adminService.createFlight(planeNumber, depCityName, arrCityName, depTime, arrTime, pType);
		}
		
		if(request.getParameter("delete") != null) {
			for(int i = 0; i < this.adminService.getFlights().size(); i++) {
				if(request.getParameter(this.adminService.getFlights().get(i).getId() + "Checkbox") != null) {
					flights = this.adminService.deleteFlight(this.adminService.getFlights().get(i).getId());
				}
			}
		}
		
		if(request.getParameter("update") != null) {
			for(int i = 0; i < this.adminService.getFlights().size(); i++) {
				if(request.getParameter(this.adminService.getFlights().get(i).getId() + "Checkbox") != null) {
					flights = this.adminService.update(toUpdate, updateDrop, this.adminService.getFlights().get(i).getId());
				}
			}
		}
		
		request.getSession().setAttribute("flights", flights);
		
		response.sendRedirect("admin");
	}

}
