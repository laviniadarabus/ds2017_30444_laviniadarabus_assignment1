package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "city")
public class City {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "latitude")
	private double latitude;
	
	@Column(name = "longitude")
	private double longitude;

	@Column(name = "name")
	private String name;
	
	@OneToMany(mappedBy="arrivalStation",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<Flight> arrivingFlights;
	
	@OneToMany(mappedBy="departureStation",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<Flight> departingFlights;
	
	public City() {
		
	}
	
	public City(String name, double latitude, double longitude) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.arrivingFlights = new ArrayList<Flight>();
		this.departingFlights = new ArrayList<Flight>();
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Flight> getArrivingFlights() {
		return arrivingFlights;
	}

	public void setArrivingFlights(List<Flight> arrivingFlights) {
		this.arrivingFlights = arrivingFlights;
	}

	public List<Flight> getDepartingFlights() {
		return departingFlights;
	}

	public void setDepartingFlights(List<Flight> departingFlights) {
		this.departingFlights = departingFlights;
	}

}
