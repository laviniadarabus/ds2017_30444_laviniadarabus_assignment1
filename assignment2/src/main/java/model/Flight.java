package model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flight")
public class Flight implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name = "number")
	private long number;
	
	@Column(name = "airplaneType")
	private String airplaneType;
	
	@ManyToOne
	@JoinColumn(name = "departureStation", nullable=true)
	private City departureStation;
	
	@ManyToOne
	@JoinColumn(name = "arrivalStation", nullable=true)
	private City arrivalStation;
	
	
	@Column(name = "departure")
	private String departure;
	
	@Column(name = "arrival")
	private String arrival;
	
	public Flight() {
		
	}
	
	public Flight(long number, City departureStation, City arrivalStation, String departure, String arrival, String airplaneType) {
		this.number = number;
		this.departureStation = departureStation;
		this.arrivalStation = arrivalStation;
		this.departure = departure;
		this.arrival = arrival;
		this.airplaneType = airplaneType;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public City getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(City departureStation) {
		this.departureStation = departureStation;
	}

	public City getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(City arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getDeparture() {
		return this.departure;
	}

	public void setDepature(String depature) {
		this.departure = depature;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	

}
