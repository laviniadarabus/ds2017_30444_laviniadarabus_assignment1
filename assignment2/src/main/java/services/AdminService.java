package services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.cfg.Configuration;

import model.City;
import model.Flight;
import repositories.CityRepository;
import repositories.FlightRepository;

public class AdminService {

	private FlightRepository flightRepository;
	private CityRepository cityRepository;

	public AdminService() {
		this.flightRepository = new FlightRepository((new Configuration().configure().buildSessionFactory()));
		this.cityRepository = new CityRepository((new Configuration().configure().buildSessionFactory()));
	}

	public List<Flight> getFlights() {
		return this.flightRepository.find();
	}

	public List<Flight> searchByDepCity(String cityName) {
		List<Flight> all = this.getFlights();
		List<Flight> result = new ArrayList<Flight>();
		Iterator<Flight> it = all.iterator();
		while (it.hasNext()) {
			Flight f = it.next();
			if (f.getDepartureStation().getName().equals(cityName)) {
				result.add(f);
			}
		}
		return result;
	}

	public List<Flight> searchByArrivalTime(String arrivalTime) {
		List<Flight> all = this.getFlights();
		List<Flight> result = new ArrayList<Flight>();
		Iterator<Flight> it = all.iterator();
		if (arrivalTime != null) {
			while (it.hasNext()) {
				Flight f = it.next();
				if (f.getArrival().equals(arrivalTime)) {
					result.add(f);
				}
			}
		}
		return result;
	}

	private List<Flight> searchByPlaneType(String pType) {
		List<Flight> all = this.getFlights();
		List<Flight> result = new ArrayList<Flight>();
		Iterator<Flight> it = all.iterator();
		if (pType != null) {
			while (it.hasNext()) {
				Flight f = it.next();
				if (f.getAirplaneType().equals(pType)) {
					result.add(f);
				}
			}
		}
		return result;
	}

	private List<Flight> searchByNumber(String planeNumber) {
		List<Flight> all = this.getFlights();
		List<Flight> result = new ArrayList<Flight>();
		Iterator<Flight> it = all.iterator();
		if (planeNumber != null) {
			while (it.hasNext()) {
				Flight f = it.next();
				if (f.getNumber() == (Long.parseLong(planeNumber))) {
					result.add(f);
				}
			}
		}
		return result;
	}

	private List<Flight> searchByDepartureTime(String depTime) {
		List<Flight> all = this.getFlights();
		List<Flight> result = new ArrayList<Flight>();
		Iterator<Flight> it = all.iterator();
		if (depTime != null) {
			while (it.hasNext()) {
				Flight f = it.next();
				if (f.getDeparture().equals(depTime)) {
					result.add(f);
				}
			}
		}
		return result;
	}

	private List<Flight> searchByArrCity(String arrCityName) {
		List<Flight> all = this.getFlights();
		List<Flight> result = new ArrayList<Flight>();
		Iterator<Flight> it = all.iterator();
		if (arrCityName != null) {
			while (it.hasNext()) {
				Flight f = it.next();
				if (f.getArrivalStation().equals(arrCityName)) {
					result.add(f);
				}
			}
		}
		return result;
	}

	public List<Flight> performSearch(String planeNumber, String depCityName, String arrCityName, String depTime,
			String arrTime, String pType, String searchBy) {
		if (searchBy.equals("Departure city")) {
			return this.searchByDepCity(depCityName);
		}
		if (searchBy.equals("Arrival city")) {
			return this.searchByArrCity(arrCityName);
		}
		if (searchBy.equals("Arrival time")) {
			return this.searchByArrivalTime(arrTime);
		}
		if (searchBy.equals("Departure time")) {
			return this.searchByDepartureTime(depTime);
		}
		if (searchBy.equals("Number")) {
			return this.searchByNumber(planeNumber);
		}
		if (searchBy.equals("Plane type")) {
			return this.searchByPlaneType(pType);
		}
		return new ArrayList<Flight>();
	}

	public City searchByName(String cityName) {
		List<City> all = this.cityRepository.find();
		Iterator<City> it = all.iterator();
		while (it.hasNext()) {
			City c = it.next();
			if (c.getName().equals(cityName)) {
				return c;
			}
		}
		return null;
	}

	public List<Flight> createFlight(String planeNumber, String depCityName, String arrCityName, String depTime,
			String arrTime, String pType) {
		List<Flight> all = this.getFlights();
		City depCity = this.searchByName(depCityName);
		City arrCity = this.searchByName(arrCityName);
		if (planeNumber != "" && depCityName != "" && arrCityName != "" && depTime != "" && arrTime != "" && pType != ""
				&& depCity != null && arrCity != null) {
			this.flightRepository
					.add(new Flight(Long.parseLong(planeNumber), depCity, arrCity, depTime, arrTime, pType));
		}
		return this.flightRepository.find();
	}

	public List<Flight> deleteFlight(int id) {
		Flight f = this.flightRepository.find(id);
		this.flightRepository.remove(f);
		return this.flightRepository.find();
	}

	public List<Flight> update(String toUpdate, String updateDrop, int id) {
		Flight f = this.flightRepository.find(id);
		switch(updateDrop){
		case "Number":
			f.setNumber(Long.parseLong(toUpdate));
			break;
		case "Departure city":
			City c = this.searchByName(toUpdate);
			if(c != null) {
				f.setDepartureStation(c);
			}
			break;
		case "Departure time":
			f.setDepature(toUpdate);
			break;
		case "Arrival city":
			City c2 = this.searchByName(toUpdate);
			if(c2 != null) {
				f.setArrivalStation(c2);;
			}
			break;
		case "Arrival time":
			f.setArrival(toUpdate);
			break;
		case "Plane type":
			f.setAirplaneType(toUpdate);
			break;
		}
		this.flightRepository.add(f);
		return this.flightRepository.find();
	}

}
