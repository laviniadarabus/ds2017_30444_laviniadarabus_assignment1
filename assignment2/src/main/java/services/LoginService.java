package services;

import java.util.Iterator;
import java.util.List;

import org.hibernate.cfg.Configuration;

import model.User;
import repositories.UserRepository;

public class LoginService {
	
	private UserRepository userRepository;
	
	public LoginService() {
		this.userRepository = new UserRepository(new Configuration().configure().buildSessionFactory());
	}
	
	public List<User> getAll(){
		return this.userRepository.find();
	}

	public String determineLoginType(String username, String password) {
		// TODO Auto-generated method stub
		List<User> users = this.userRepository.find();
		User u = null;
		Iterator<User> it = users.iterator();
		while(it.hasNext()) {
			u = it.next();
			if(u.getUsername().equals(username) && u.getPassword().equals(password)) {
				break;
			}
		}
		if(u != null) {
			return u.getType();
		}
		
		return "error";
		
	}

}
