package services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.cfg.Configuration;

import model.Flight;
import repositories.FlightRepository;

public class UserService {
	
	private FlightRepository flightRepository;
	
	public UserService() {
		this.flightRepository = new FlightRepository((new Configuration().configure().buildSessionFactory()));
	}

	public List<Flight> getFlights() {
		return this.flightRepository.find();
	}
	
	public List<Flight> searchByCity(String cityName){
		List<Flight> all = this.getFlights();
		List<Flight> result = new ArrayList<Flight>();
		Iterator<Flight> it = all.iterator();
		while(it.hasNext()) {
			Flight f = it.next();
			if(f.getDepartureStation().getName().equals(cityName)) {
				result.add(f);
			}
		}
		return result;
	}
	
	public List<Flight> searchByArrivalTime(String arrivalTime){
		List<Flight> all = this.getFlights();
		List<Flight> result = new ArrayList<Flight>();
		Iterator<Flight> it = all.iterator();
		if(arrivalTime != null) {
			while(it.hasNext()) {
				Flight f = it.next();
				if(f.getArrival().equals(arrivalTime)) {
					result.add(f);
				}
			}
		}
		
		return result;
	}

	public List<Flight> performSearch(String searchBy, String depCityName, String arrivalTime) {
		if(searchBy.equals("Departure city")) {
			return this.searchByCity(depCityName);
		}
		return this.searchByArrivalTime(arrivalTime);
	}

}
