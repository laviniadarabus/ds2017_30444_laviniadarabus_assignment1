package repositories;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import model.Flight;

public class FlightRepository {
	
private SessionFactory factory;
	
	public FlightRepository(SessionFactory factory) {
		this.factory = factory;
	}
	
	public Flight add(Flight flight) {
		Session session = factory.openSession();
		Transaction tx = null;
		
		tx = session.beginTransaction();
		session.saveOrUpdate(flight);
		tx.commit();
		session.close();
		return flight;
	}
	
	public void remove(Flight flight) {
		Session session = factory.openSession();
		Transaction tx = null;
		
		tx = session.beginTransaction();
		session.delete(flight);
		tx.commit();
		session.close();
	}
	
	@SuppressWarnings("unchecked")
	public Flight find(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = new ArrayList<Flight>();

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> find() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = new ArrayList<Flight>();
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights;
	}

}
