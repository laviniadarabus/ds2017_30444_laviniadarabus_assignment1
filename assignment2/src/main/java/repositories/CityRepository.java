package repositories;



import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import model.City;

public class CityRepository {
	
	private SessionFactory factory;
	
	public CityRepository(SessionFactory factory) {
		this.factory = factory;
	}
	
	public City add(City city) {
		Session session = factory.openSession();
		Transaction tx = null;
		
		tx = session.beginTransaction();
		session.saveOrUpdate(city);
		tx.commit();
		session.close();
		return city;
	}
	
	public void remove(City city) {
		Session session = factory.openSession();
		Transaction tx = null;
		
		tx = session.beginTransaction();
		session.delete(city);
		tx.commit();
		session.close();
	}
	
	@SuppressWarnings("unchecked")
	public List<City> find() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> cities = null;
		try {
			tx = session.beginTransaction();
			cities = session.createQuery("FROM City").list();
			tx.commit();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return cities;
	}
	
	@SuppressWarnings("unchecked")
	public City find(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> cities = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE id = :id");
			query.setParameter("id", id);
			cities = query.list();
			tx.commit();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return cities != null && !cities.isEmpty() ? cities.get(0) : null;
	}

}
