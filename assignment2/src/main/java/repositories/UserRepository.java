package repositories;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import model.User;

public class UserRepository {
	
	private SessionFactory factory;
	
	public UserRepository(SessionFactory factory) {
		this.factory = factory;
	}
	
	public User add(User user) {
		Session session = factory.openSession();
		Transaction tx = null;
		
		tx = session.beginTransaction();
		session.saveOrUpdate(user);
		tx.commit();
		session.close();
		return user;
	}
	
	public void remove(User user) {
		Session session = factory.openSession();
		Transaction tx = null;
		
		tx = session.beginTransaction();
		session.delete(user);
		tx.commit();
		session.close();
	}
	
	@SuppressWarnings("unchecked")
	public User find(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;

		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE id = :id");
			query.setParameter("id", id);
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> find() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			users = session.createQuery("FROM User").list();
			tx.commit();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users;
	}

}
